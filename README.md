Docker in WSL Windows 2010 Home
===

Introduction
---

I decided to start using the `Windows Subsystem for Linux` (WSL) for developing on Linux and `docker`. Especially the latter took some effort to set up. This repository simplyfies it.

Prerequisites
---

This repository assumes you have WSL up and running. There is plenty of resources to help you with that.

This repository is created for debian distributions. It has been tested in WSL `Ubuntu`. It should be relatively easy to make it work for the `Redhat` family, but that is not included in this repo.

Setting up docker
---

`docker` is set up using an `ansible` playbook. So before being able to install `docker`, `ansible` must be installed first. This is all covered with the following command:

```bash
make setup run
```

Documentation
---

### Makefile

The `Makefile` has the following targets:

* `setup`: Setup installs `ansible`, because `ansible` makes it much more easy to install `docker`.
* `run`: Runs the playbook to install `docker`.
* `run-new`: This target is only useful for development. In an `ansible playbook` you can add `tags` to `roles` and `tasks` and choose to only execute `tasks` that have a specific tag. If you assign the tag "new" to `roles` or `tasks` that you want run and then call `make run-new`, it will only execute those tasks.

### Ansible Playbook

#### Roles

This playbook has the following `Roles`.

##### download_facts

Whenever a playbook is run `ansible` will first gather facts about each server. These facts can be quite useful when making decisions in your playbooks, but it's not always obviously exactly what facts are available. This role downloads the facts and saves them as `artifacts.yml`.

##### docker

This role installs the packages required by `docker`, adds the docker repo and installs `docker`.

##### docker-machine

This repo is created to help fix the issue where running `docker` on Windows 10 Home is problematic. One way to make it possible is with `docker-machine`. The binary is installed in `c:\Users\Username\bin` (which is `/mnt/c/Users/Username/bin` in WSL). This role adds that directory to WSL's path.
