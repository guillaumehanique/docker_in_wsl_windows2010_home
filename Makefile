.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    setup        Setup the Ubuntu WSL environment so that Ansible Playbooks can be executed."
	@echo "    run          Run the playbook."
	@echo "    run-new      Run the playbook, but only the roles that are marked 'new'."

.PHONY: setup
setup:
	sudo apt-get update
	sudo apt-get install -y ansible

.PHONY: run
run:
	ansible-playbook -i hosts.yml playbook.yml

.PHONY: run-new
run-new:
	ansible-playbook -i hosts.yml playbook.yml --tags new
